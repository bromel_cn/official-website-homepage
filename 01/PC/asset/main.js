let slider = new Swiper("#slide-Show", {
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
});

let partner = new Swiper("#index-partner", {
  slidesPerView: 3,
  spaceBetween: 5,
  autoplay:true,
  grid: {
    fill: 'rows',
    rows: 2,
  },
  pagination: {
    el: '.swiper-pagination',
    clickable :true,
  },
});

let news = new Swiper("#news", {
  autoplay:true,
  pagination: {
    el: '.swiper-pagination',
    clickable :true,
  },
});

{
  let tabs = document.querySelectorAll('.tabs li')
  let main = document.querySelectorAll('.tabs-main li')
  
  for (let index = 0; index < tabs.length; index++) {
    
    main[index].classList.remove('active')
    main[0].classList.add('active')

    tabs[index].classList.remove('active')
    tabs[0].classList.add('active')

    tabs[index].addEventListener('mouseover', function(){
      for (let i = 0; i<tabs.length; i++){
        tabs[i].classList.remove('active')
        main[i].classList.remove('active')
      }
      tabs[index].classList.add('active')
      main[index].classList.add('active')
    })    
  }
}
